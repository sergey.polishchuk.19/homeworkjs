"use strict"

const buttonItem = document.querySelectorAll('.tabs-title');
const tabsItems = document.querySelectorAll('.box')
buttonItem.forEach(element =>{
    element.addEventListener('click', event => {
        let currentBtn = element;
        let tabId = currentBtn.dataset['filter'];
        let currentTab = document.querySelector(tabId);

        
        buttonItem.forEach(element=>{
            element.classList.remove('active')
        })
        tabsItems.forEach(element=>{
            element.classList.remove('active')
        })
        currentBtn.classList.add('active')
        currentTab.classList.add('active')
    })
})
