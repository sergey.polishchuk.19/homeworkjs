'use strict'

$(document).ready(function(){
  $("#main").on("click","a", function (event) {
      event.preventDefault();
      let id  = $(this).attr('href'),
          top = $(id).offset().top;
      $('body,html').animate({scrollTop: top}, 1500);
  });
});

$(document).ready(function() {
  let btn = $('#button');  
  $(window).scroll(function() {     
    if ($(window).scrollTop() > 800) {
       btn.addClass('show');
     } else {
       btn.removeClass('show');
     }
   });
   btn.on('click', function(e) {
     e.preventDefault();
     $('html, body').animate({scrollTop:0}, '800');
   });
});

$(document).ready(function slideToggle(){
  $('#hide-block').click(function(){
    $('.gallery').slideToggle(300);
    return false
  });
});

