"use strict"

let btn = document.querySelectorAll(".btn");

document.addEventListener("keydown", e => {
  btn.forEach( item => {
    if (e.key.toUpperCase() === item.dataset.name.toUpperCase()) {
      item.classList.add("color");
    } else {
      item.classList.remove("color");
    }
  });
});
