"use strict"

 function createNewUser() {
   let firstName = prompt("Enter your name!"),
   lastName = prompt("Enter your last name!"),
   birthday = prompt("When's your birthday?", "dd.mm.yyyy").split('.'),
   birthDate = new Date(birthday[2], birthday[1], birthday[0]),
   now = new Date();
   let newUser = {
      firstName,
      lastName,
      birthday,
      birthDate,
      getLogin: function () {
         return (this.firstName[0] + this.lastName).toLowerCase();
      },
      getAge: function () {
         let age = Math.floor((Date.now() - birthDate.getTime()) / 1000 / (60 * 60 * 24) / 365.2425);
            return age;
      },
      getPassword: function () {
         return this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + this.birthDate.getFullYear();
      },
   };
   return newUser;
};
let infoUser = createNewUser();
console.log(infoUser.getLogin());
console.log(infoUser.getAge());
console.log(infoUser.getPassword());



