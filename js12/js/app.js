"use strict"

const img = document.querySelectorAll('img');
let showList = 0;
let sliderWork = false;

function slider(){
  img[showList].className = 'image-to-show';
  showList = (showList + 1) % img.length;
  img[showList].classList.add('show')
}

let sliderShow = setInterval(slider, 3000, (sliderWork = true));

const stop = document.querySelector(".pause");
stop.addEventListener("click", pause);

function pause() {
  clearInterval(sliderShow);
  sliderWork = false;
}

const listGo = document.querySelector(".play");
listGo.addEventListener("click", next);

function next() {
  if (!sliderWork) {
    sliderShow = setInterval(slider, 3000, (sliderWork = true));
  }
}
