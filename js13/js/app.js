'use strict'

const btn = document.querySelector('.theme'); 
const element = document.querySelector('.style');  
function changeTheme() {
    if (element.getAttribute('href') === "./css/light.css") {
        element.setAttribute('href', "./css/dark.css");
        localStorage.setItem('href', './css/dark.css');
    } else {
        element.setAttribute('href', "./css/light.css");
        localStorage.setItem('href', './css/light.css');
    }
}
if (localStorage.getItem('href') === null) {
    localStorage.setItem('href', './css/light.css');
    console.log(localStorage.getItem('href'))
} else {
    const style = localStorage.getItem('href');
    document.querySelector('.style').setAttribute('href', `${style}`);
}

btn.addEventListener('click', changeTheme);

