"use strict"

let input = document.querySelector('input');
let btn = document.createElement('button')
let span = document.createElement('span')
input.addEventListener('focus', ()=>{
   input.style.border = '3px solid #62aa2a'
})
input.addEventListener('blur', ()=>{
   if(input.value > 0){
      span.innerHTML = `Текущая цена: ${input.value}`;
      document.body.before(span);
      input.style.color = '#62aa2a';
      btn.classList.add(".closebtn");
      btn.textContent = 'X'
      span.append(btn)
   }else{
      input.style.border = '3px solid #FF2C00'
      span.textContent = "Please enter correct price";
      document.body.after(span);
   }
})
btn.addEventListener("click", () => {
   input.value = '';
   span.remove();
   btn.remove();
})